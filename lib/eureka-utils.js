exports.id = 'lib/eureka-utils';

const request = require('request-promise');
const _ = require('underscore');
const Eureka = require('eureka-js-client').Eureka;
const myIp = require('ip').address();

const getApplications = async (eurekaUrl, proxy, pattern) => {
  let response = await request({
    url: eurekaUrl,
    headers: {
      'Accept': 'application/json'
    },
    json: true,
    proxy: proxy ? proxy : false
  });

  if (!pattern) {
    return response.applications.application;
  }

  let regexp = new RegExp(pattern, 'i');
  return _.filter(response.applications.application, app => regexp.test(app.name));
};

// Service must provide a name and a port
const registerLocalApplication = (service) => {
  let client = new Eureka({
    instance: {
      instanceId: 'eureka-synschronizer:' + service.name,
      // instanceId: 'eureka-synschronizer:' + service.name + ':'+service.port,
      app: service.name,
      hostName: myIp,
      ipAddr: myIp,
      port: {
        '$': service.port,
        '@enabled': 'true',
      },
      vipAddress: service.name.toLowerCase(),
      healthCheckUrl: 'http://' + myIp + ':' + service.port + '/health',
      homePageUrl: 'http://' + myIp + ':' + service.port + '/',
      statusPageUrl: 'http://' + myIp + ':' + service.port + '/info',
      dataCenterInfo: {
        name: 'MyOwn',
        '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo'
      },
      metadata: {
        isLocalProxy: true
      }
    },
    eureka: {
      // eureka server host / port
      host: '127.0.0.1',
      port: 8761,
      servicePath: '/eureka/apps/'
    }
  });

  client.start();
  return client;
};

const getLocalApplicationWithoutProxies = async () => {
  const localApplications = await getApplications(
      'http://127.0.0.1:8761/eureka/apps');

  return _.filter(localApplications, app => {
    // Keep applications that have at least one server that run locally
    return _.filter(app.instance, instance => {
      // Keep instances that are not local proxy
      return !instance.metadata || !instance.metadata.isLocalProxy;
    }).length > 0;
  });
};

// exports
exports.registerLocalApplication = registerLocalApplication;
exports.getApplications = getApplications;
exports.getLocalApplicationWithoutProxies = getLocalApplicationWithoutProxies;

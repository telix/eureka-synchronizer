# Eureka Proxy

Declare in local eureka all services of another eureka with http proxies to each service.

# Usage

## Installing
```
npm install -g eureka-synchronizer
```

## Start
```
eureka-synchronizer
```

```
  Usage: eureka-synchronizer [options] <eureka>

  Synchronize service available on a distant eureka with the local eureka by creating http proxies to distant services


  Options:

    -V, --version              output the version number
    -i, --interval <interval>  The interval to refresh proxies in seconds. Default 60 sec (default: 60)
    -x, --proxy <proxy>        The proxy to use to contact the eureka
    -p, --port <port>          The first port ot bind proxies. Default 9000 (default: 9000)
    -f, --filter <filter>      Allow to filter, with a regexp, the distant services to bind.
    -h, --help                 output usage information

```

### Example

#### Example with a proxy
```
eureka-synchronizer --proxy http://proxy.corporate.com:3128 http://eureka.dev.net/eureka/apps
```

#### Example with interval, port and filter (name which start with `CORP_SERV_`)
```
eureka-synchronizer -i 30 -p 9000 -f "^CORP_SERV_" http://eureka.dev.net/eureka/apps
```

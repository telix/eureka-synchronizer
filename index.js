#!/usr/bin/env node

const eurekaUtils = require('./lib/eureka-utils');
const _ = require('underscore');
const httpProxy = require('http-proxy');
const HttpsProxyAgent = require('https-proxy-agent');
const program = require('commander');

// Global variables
let nextPortAvailable;
let proxies = [];

const repeat = (action, interval) => {
  action();
  setInterval(action, interval);
};

program
    .version('1.0.0')
    .description(
        'Synchronize service available on a distant eureka with the local eureka by creating http proxies to distant services')
    .arguments('<eureka>')
    .option('-i, --interval <interval>', 'The interval to refresh proxies in seconds. Default 60 sec.', 60)
    .option('-x, --proxy <proxy>', 'The proxy to use to contact the eureka.')
    .option('-p, --port <port>', 'The first port to bind proxies. Default 9000', 9000)
    .option('-f, --filter <filter>', 'Allow to filter, with a regexp, the distant services to bind.')
    .action((eureka, options) => {
      repeat(async () => {
        console.log('\n\n    - Refresh %s -\n\n', new Date());

        nextPortAvailable = options.port;

        const startedApps = await eurekaUtils.getLocalApplicationWithoutProxies();
        const startedAppNames = _.map(startedApps, app => app.name);
        console.log('Find %s started applications in local : %s',
            startedAppNames.length, startedAppNames);

        const remoteApps = await eurekaUtils.getApplications(eureka,
            options.proxy, options.filter);
        const remoteAppNames = _.map(remoteApps, app => app.name);
        console.log('Find %s remote applications : %s', remoteAppNames.length,
            remoteAppNames);

        const alreadyStarted = _.map(proxies, service => service.name);
        console.log('Find %s proxies alreadyStarted : %s',
            alreadyStarted.length, alreadyStarted);

        const appNamesToProxy = _.difference(remoteAppNames,
            _.union(startedAppNames, alreadyStarted));
        console.log('Find %s applications to proxy : %s',
            appNamesToProxy.length, appNamesToProxy);

        // Applications that disappeared on distant eureka
        // And applications started locally and that have a Proxy
        const appNamesToStop = _.union(
            _.difference(alreadyStarted, remoteAppNames),
            _.intersection(alreadyStarted, startedAppNames));
        console.log('Find %s proxies to stop : %s', appNamesToStop.length,
            appNamesToStop);

        const appsToProxy = _.filter(remoteApps,
            app => _.indexOf(appNamesToProxy, app.name) !== -1);
        _.each(appsToProxy, app => {

          let service = {
            name: app.name,
            port: nextPortAvailable++
          };

          // Create proxy
          console.log('Creating proxy for app %s on port %s', service.name,
              service.port);

          const port = app.instance[0].port.$;
          const hostname = app.instance[0].hostName;

          let proxyServer = {
            target: 'http://' + hostname + ':' + port
          };

          if(options.proxy) {
            proxyServer.agent = new HttpsProxyAgent(options.proxy);
          }

          service.httpProxy = httpProxy.createProxyServer(proxyServer);
          service.httpProxy.on('proxyReq',
              (proxyReq, req) => console.log('%s > Start request : %s %s',
                  service.name, proxyReq.method, req.url));
          service.httpProxy.on('proxyRes',
              (proxyRes, req, res) => console.log('%s > Response %s',
                  service.name, proxyRes.statusCode));
          service.httpProxy.listen(service.port);

          console.log('Proxy created : localhost:%s -> %s:%s', service.port,
              hostname, port);

          // Register proxy in local eureka
          console.log('Registering service %s in local eureka', service.name);

          service.eurekaClient = eurekaUtils.registerLocalApplication(service);

          // Store Proxy
          proxies.push(service);

        });

        const proxiesToStop = _.filter(proxies,
            service => _.indexOf(appNamesToStop, service.name) !== -1);
        _.each(proxiesToStop, app => {
          console.log('Stopping %s service', app.name);

          // Stop proxy
          if (app.httpProxy) {
            console.log('Stopping http proxy of %s', app.name);
            app.httpProxy.close();
            delete app.httpProxy;
          }

          // Stop eureka client
          if (app.eurekaClient) {
            console.log('Stopping eureka client of %s', app.name);
            app.eurekaClient.stop();
            delete app.eurekaClient;
          }

          // Remove of storage
          const index = _.indexOf(proxies, app);
          if (index !== -1) {
            console.log('Remove proxy of service %s', app.name);
            proxies.splice(index, 1);
          } else {
            console.error('Cannot find proxy %s', app.name);
          }
        });

      }, options.interval * 1000);

      process.on('SIGINT', () => {
        _.each(proxies, app => {
          console.log('Stopping %s service', app.name);

          // Stop proxy
          if (app.httpProxy) {
            console.log('Stopping http proxy of %s', app.name);
            app.httpProxy.close();
            delete app.httpProxy;
          }

          // Stop eureka client
          if (app.eurekaClient) {
            console.log('Stopping eureka client of %s', app.name);
            app.eurekaClient.stop();
            delete app.eurekaClient;
          }
        });

        setInterval(() => process.exit(0), 1000);
      });
    });

program.parse(process.argv);

if (process.argv.length < 3) {
  program.help();
}